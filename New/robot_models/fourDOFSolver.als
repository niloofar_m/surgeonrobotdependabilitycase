module fourDOFsolver

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	ButtonInterface_instance +
	IKSolver_plugin in loads
}

one sig IKSolver_family extends SolverFamily{}{
	calls = IKSolver
}

one sig IKSolver extends KinematicModel{}

one sig fourDOFsolver extends ArmType{}{
	inverseKSolver = IKSolver
}

one sig fourDOFsolverArm extends RobotArm{}{
	armModel = fourDOFsolver
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsEnabled
}

one sig Current_Robot extends Robot {}{
	arms = fourDOFsolverArm
}

fact{
    #ArmType.anglelimit = 3
    #RobotControl.output = 3
	#solverResult = 3
}

check ArmAngleCorrect for 4 but 8 Plugin
