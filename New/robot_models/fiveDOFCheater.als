module fiveDOFCheater

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

--list of loaded plugins - get the list of plugins extracted from config file
one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	ButtonInterface_instance +
	IKSolver_plugin in loads
}

one sig IKSolver_family extends SolverFamily{}{
	calls = IKSolver5DOF
}

one sig IKSolver5DOF extends KinematicModel{}

one sig fiveDOFCheater extends ArmType{}{
	inverseKSolver = IKSolver5DOF
}

one sig fiveDOFCheaterArm extends RobotArm{}{
	armModel = fiveDOFCheater
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsEnabled
}

one sig Current_Robot extends Robot {}{
	arms = fiveDOFCheaterArm
}

fact{
    #ArmType.anglelimit = 4
    #RobotControl.output = 4
	#solverResult = 4
}

check ArmAngleCorrect for 5 but 8 Plugin
