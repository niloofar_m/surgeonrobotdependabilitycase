module frankenVREP

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	IKSolver_plugin +
	ButtonInterface_instance
	in loads
}

one sig IKSolver_family extends SolverFamily{}{
	calls = FrankenBot
}

one sig FrankenBot extends KinematicModel{}

one sig FrankenVREP extends ArmType{}{
	inverseKSolver = FrankenBot
}

one sig FrankenVREPArm extends RobotArm{}{
	armModel = FrankenVREP
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsDisabled
}

one sig Current_Robot extends Robot {}{
	arms = FrankenVREPArm
}

fact{
    #ArmType.anglelimit = 4
    #RobotControl.output = 4
	#solverResult = 4
}

check ArmAngleCorrect for 5 but 8 Plugin
