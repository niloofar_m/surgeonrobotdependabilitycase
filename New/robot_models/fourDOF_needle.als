module fourDOF_needle

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	ButtonInterface_instance +
	IKSolver_plugin in loads
}

one sig IKSolver_family extends SolverFamily{}{
	calls = IKSolver
}

one sig IKSolver extends KinematicModel{}

one sig fourDOF_needle extends ArmType{}{
	inverseKSolver = IKSolver
}

one sig fourDOF_needleArm extends RobotArm{}{
	armModel = fourDOF_needle
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsDisabled
}

one sig Current_Robot extends Robot {}{
	arms = fourDOF_needleArm
}

fact{
    #ArmType.anglelimit = 3
    #RobotControl.output = 3
	#solverResult = 3
}

check ArmAngleCorrect for 4 but 8 Plugin
