module markBot

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig TwoArmKinematics_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	TwoArmKinematics_plugin +
	ButtonInterface_instance in loads
}

one sig SingleArmFamily extends SolverFamily{}{
	calls = CombinedBot
}

one sig CombinedBot extends KinematicModel{}

one sig markBot extends ArmType{}{
	inverseKSolver = CombinedBot
}

one sig markBotArm extends RobotArm{}{
	armModel = markBot
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsEnabled
}

one sig Current_Robot extends Robot {}{
	arms = markBotArm
}

--3 for each arm, 6 angles overall
fact{
    #ArmType.anglelimit = 6
    #RobotControl.output = 6
	#solverResult = 6
}

check ArmAngleCorrect for 7 but 8 Plugin
