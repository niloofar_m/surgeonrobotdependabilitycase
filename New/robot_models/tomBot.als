module tomBot

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig Kinematics_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	Kinematics_plugin +
	ButtonInterface_instance in loads
}

one sig SingleArmFamily extends SolverFamily{}{
	calls = TwoArmCoupledShoulder
}

one sig TwoArmCoupledShoulder extends KinematicModel{}

one sig tomBot extends ArmType{}{
	inverseKSolver = TwoArmCoupledShoulder
}

one sig tomBotArm extends RobotArm{}{
	armModel = tomBot
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsDisabled
}

one sig Current_Robot extends Robot {}{
	arms = tomBotArm
}

fact{
    #ArmType.anglelimit = 3
    #RobotControl.output = 3
	#solverResult = 3
}

check ArmAngleCorrect for 4 but 8 Plugin
