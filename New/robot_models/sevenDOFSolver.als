module sevenDOFSolver

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	IKSolver_plugin +
	ButtonInterface_instance in loads
}

one sig IKSolver_family extends SolverFamily{}{
	calls = IKSolver
}

one sig IKSolver extends KinematicModel{}

one sig sevenDOFSolver extends ArmType{}{
	inverseKSolver = IKSolver
}

one sig sevenDOFSolverArm extends RobotArm{}{
	armModel = sevenDOFSolver
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsDisabled
}

one sig Current_Robot extends Robot {}{
	arms = sevenDOFSolverArm
}

fact{
    #ArmType.anglelimit = 7
    #RobotControl.output = 7
	#solverResult = 7
}

check ArmAngleCorrect for 8 but 8 Plugin
