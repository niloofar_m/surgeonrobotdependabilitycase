module twoArmLouBot

open metamodel/SurgeonBot

sig armangle extends ArmAngle{}
sig xyz_input extends Coordinate{}

--plugins expected from a typical config file
one sig GeomagicTouchPlugin_instance extends GeomagicTouch_plugin{}
one sig HomePosition_instance extends HomePosition{}
one sig Clutch_instance extends Clutch_Plugin{}
one sig TwoArmKinematics_plugin extends SolverPlugin{}
one sig ButtonInterface_instance extends ButtonInterface{}

one sig loaded_plugins_of_ extends LoadedPlugins {}{
	GeomagicTouchPlugin_instance +
	HomePosition_instance + 
	Clutch_instance +
	ButtonInterface_instance +
	TwoArmKinematics_plugin in loads
}

one sig SingleArmFamily extends SolverFamily{}{
	calls = TwoArmCoupledShoulder3DOF
}

one sig TwoArmCoupledShoulder3DOF extends KinematicModel{}

one sig twoArmLouBot extends ArmType{}{
	inverseKSolver = TwoArmCoupledShoulder3DOF
}

one sig twoArmLouBotArm extends RobotArm{}{
	armModel = twoArmLouBot
}

one sig UsedGeomagicTouch extends GeomagicTouch {}{
	force = HapticsEnabled
}

one sig Current_Robot extends Robot {}{
	arms = twoArmLouBotArm
}

--3 for each arm, 6 angles overall
fact{
    #ArmType.anglelimit = 6
    #RobotControl.output = 6
	#solverResult = 6
}

check ArmAngleCorrect for 7 but 8 Plugin
