module SurgeonRobot

open SurgeonBot

sig armangle_lou extends ArmAngle{}
sig xyz_input_co extends Coordinate{}

--plugins extracted from config file. they are specified
--each of th
one sig gmt_plugin extends GeomagicTouch_plugin{}
one sig hp_instance extends HomePosition{}
one sig clutch_instance extends Clutch{}
sig IKSolver_plugin extends SolverPlugin{}{
	solverfamily = IKSolver_family
}

--list of loaded plugins - get the list of plugins extracted from config file
one sig loaded_plugins_of_ extends LoadedPlugins {}{
	gmt_plugin + hp_instance + clutch_instance + IKSolver_plugin in loads
}

one sig rbt_app extends RobotApp {}

one sig IKSolver_family extends SolverFamily{}{
	calls = IKSolver //name of solver class
}

one sig IKSolver extends KinematicModel{}

one sig LouBizzle extends RobotModel {}{
	inverseKSolver = IKSolver
}

//one sig LouBizzleArm *
one sig loaded_arm extends RobotArm{}
{
	kinModel = LouBizzle
}

one sig gmt extends GeomagicTouch {}{
	force = force_feedback
}

one sig force_feedback extends No{} //Set this either to No or Yes

one sig current_robot extends Robot {}{
	arms = loaded_arm //arms = LouBizzleArm *
}

one sig rc extends RobotControl{}

--If the force output is no, force feedback extends No
--if the force output is yes, force feedback extends Yes

check ArmAngleCorrect for 4 but 9 {lugin
