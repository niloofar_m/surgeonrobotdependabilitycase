module plugins

open SurgeonBot

-- The plugins necessary to load 

sig ButtonInterface extends Plugin{}
sig AlphaAngle extends Plugin{}
sig Clutch extends Plugin{
--reset: Coordinate -> one Coordinate
}
sig Scale extends Plugin{}
sig GrasperLimits extends Plugin{}
sig DummyController extends Plugin{}
sig GeomagicTouch_plugin extends Plugin{}
sig HomePosition extends Plugin{}


--one sig IKSolver_plugin extends SolverPlugin{}
--one sig Kinematics extends SolverPlugin{}
--one sig TwoArmKinematics extends SolverPlugin{}

--one sig IKSolver_family extends SolverFamily{}
--one sig SingleArm_family extends SolverFamily{}
--one sig TwoArm_family extends SolverFamily{}

--one sig CombinedBot extends KinematicModel{}
--one sig CoupledShoulder3DOF extends KinematicModel{}
--one sig CoupledShoulderAndElbow3DOF extends KinematicModel{}
--one sig IKSolver extends KinematicModel{}
--one sig IKSolver5DOF extends KinematicModel{}
--one sig TwoArmCoupledShoulder extends KinematicModel{}
--one sig TwoArmCoupledShoulder3DOF extends KinematicModel{}


--total distinct RobotModels: 16
--total distinct plugins 
