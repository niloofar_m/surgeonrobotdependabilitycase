module SurgeonRobot

--components

sig ArmAngle, Coordinate {}

sig GeomagicTouch {
	input: some Coordinate,
	gmtaction, gmtsignal : GeomagicEvents
}

one sig RobotApp {
	msgin : GeomagicEvents,
	msgout: RobotAppEvents
}

sig KinematicModel {
	anglelimit: set ArmAngle, --set of all the arm angles that are less than limit
	inverseKSolver: one Solver
}

one sig Robot {
arms: some RobotArm,
}

sig RobotArm
{
kinModel: one KinematicModel,
	finalAngleSignal: one RobotControlEvent,
	output: some ArmAngle
}

sig Solver{
	inputcoordinates : SolverEvent,
	solverResult: Coordinate -> ArmAngle
}

sig RobotControl{
//sends the signal to robot joints
	robotMsgIn: some RobotAppEvents,
	robotMsgOut: some RobotControlEvent
}

/*events*/
sig Event {}

-- events related to GeomagicTouch device
abstract sig GeomagicEvents extends Event {} //just movement
one sig GMTDeviceAction extends GeomagicEvents {}
one sig GMTDeviceSignal extends GeomagicEvents {}

-- events related to RobotApp
abstract sig RobotAppEvents extends Event {}
one sig MsgToARobotApp extends RobotAppEvents{}
one sig MsgToOther extends RobotAppEvents{}

-- events related to Kinematics
abstract sig SolverEvent extends Event {}
one sig CoordinatesEvent extends SolverEvent{}
--sig JointAngleEvent extends SolverEvent{}

-- signatures describing the events relating to robot control 
abstract sig RobotControlEvent extends Event {}
one sig SignalFromRobotApp extends RobotControlEvent{}
one sig SignalToRobot extends RobotControlEvent{}

//pedal press/not press - later

--return the angles produced from a specific coordinate
fun getArmAngles[s: Solver, c: Coordinate] : one (ArmAngle) { 
	s.solverResult[c]
}

--Facts.
--outputs should be in the range of solverResult
fact{
all o: RobotArm.output | one a: getArmAngles[Solver,Coordinate] | o = a
}

//There is one kinematic model for each robot arm
fact {
    all r: RobotArm | one k: KinematicModel | r.kinModel = k
}

//Each solver belongs to one kinematic model
fact {
   Solver in KinematicModel.*inverseKSolver
}

//all coordinates belong to GMT movements
fact {
	all c: Coordinate | all g : GeomagicTouch | c in g.input 
}

//for each of the c coordinates there exists an angle and that angle is in the solver result 
fact {
	all c: Coordinate | one a: ArmAngle, s : Solver| c->a in s.solverResult
}

--no angle output is possible for 2 different coordinates. but is that true though?

--High-level property
pred CorrectAngleProperty [gmt : GeomagicTouch, angle : RobotArm] {
  gmt.input = Coordinate => angle.output = ArmAngle
}

/*assumptions on components */
--the geomagic touch movement event results in a signal
--action -> signal
pred GMTMovement [gmt : GeomagicTouch]
{
	gmt.gmtaction = GMTDeviceAction =>gmt.gmtsignal = GMTDeviceSignal
}

-- the signal sends a coordinate to RobotApp
-- msg that was recieved is the one sent to other
pred RobotAppGet [robotapp : RobotApp]
{
	robotapp.msgin = GMTDeviceSignal => robotapp.msgout = MsgToOther
}

pred GetCoordinates [gmt : GeomagicTouch, solver: Solver]
{
	gmt.input = Coordinate => solver.inputcoordinates = CoordinatesEvent
}

pred RobotControlEvent [rce: RobotControl]
{
	rce.robotMsgIn = MsgToOther => rce.robotMsgOut = SignalToRobot
}

pred SignalToRobot [robotArm: RobotArm]
{
	robotArm.finalAngleSignal = SignalToRobot
}

--for 2nd draft, two of each, but one robot
fact {
	one Robot
	one KinematicModel
	one GeomagicTouch
	one RobotControl
	one RobotArm
}

pred properties []{
--the assumptions are satisfied
	RobotAppGet[RobotApp]
	GetCoordinates[GeomagicTouch, Solver]
	RobotControlEvent[RobotControl]
	SignalToRobot[RobotArm]
	GMTMovement[GeomagicTouch]
}

--Not sure if properties should be called in a fact. 
--But we are assuming that all the events are handled correctly?
fact {
	#KinematicModel.anglelimit > 0
	properties[]
}

--assert if the arm angle created by movement is in the set of armangle limit
assert ArmAngleCorrect {
all a: RobotArm.output | a in KinematicModel.anglelimit
}
check ArmAngleCorrect for 2

--note to self: what you should get as output
-- solverResult[Coordinate1] is equal to ArmAngle0 unless ArmAngle0 is not in anglelimit
