module frankenvrep

open SurgeonBotFeatureSpec2

sig armangle extends ArmAngle{}
sig xyz_input_coordinate extends Coordinate{}

--plugins expected from a typical config file
one sig gmt_plugin extends GeomagicTouch_plugin{}
one sig hp_instance extends HomePosition{}
one sig clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}
one sig grasper_limits extends GrasperLimits{}

--list of loaded plugins - get the list of plugins extracted from config file
one sig loaded_plugins_of_ extends LoadedPlugins {}{
	gmt_plugin + hp_instance + clutch_instance + IKSolver_plugin in loads
}
//How can I specify that the only ones in loads are these?

--name of solver class
one sig IKSolver_family extends SolverFamily{}{
	calls = FrankenBot
}

one sig FrankenBot extends KinematicModel{}

one sig FrankenVREP extends ArmType{}{
	inverseKSolver = FrankenBot
}

--one sig LouBizzleArm
one sig FrankenVREPArm extends RobotArm{}
{
	armModel = FrankenVREP
}

one sig gmt extends GeomagicTouch {}{
	force = HapticsDisabled
}

one sig current_robot extends Robot {}{
	arms = FrankenVREPArm
}

check ArmAngleCorrect for 4 but 8 Plugin
