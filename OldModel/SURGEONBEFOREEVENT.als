module SurgeonRobot

-- A distinction between whether or not a solver family is capable of producing notification
-- or if the current robot arm DOES produce notification

--components

abstract sig ArmAngle, Coordinate, Controller {}

abstract sig Plugin {
	pluginInput: one Event,
	pluginOutput: one Event,
}

abstract sig SolverFamily{
	calls: one KinematicModel
}

abstract sig GeomagicTouch {
	input: some Coordinate,
	gmtsignal : GeomagicEvent,
	force: Notification
}

abstract one sig RobotApp {
	includes: set Plugin
}

abstract one sig LoadedPlugins {
	loads: set Plugin
}

abstract sig RobotModel {
	anglelimit: set ArmAngle, 
--set of all the arm angles that are less than limit
	inverseKSolver: one KinematicModel
}

abstract one sig Robot {
	arms: some RobotArm
}

abstract sig RobotArm
{
	kinModel: one RobotModel,
	finalAngleSignal: one RobotControlEvent,
}

abstract sig SolverPlugin extends Plugin{
	solverfamily: one SolverFamily,
}

abstract sig KinematicModel{
	solverResult: Coordinate -> ArmAngle
}

abstract sig RobotControl{
	robotConfig: some Controller,	
	output: some ArmAngle
}

one sig ButtonInterface extends Plugin{}
one sig AlphaAngle extends Plugin{}
one sig Clutch extends Plugin{}
one sig Scale extends Plugin{}
one sig GrasperLimits extends Plugin{}
one sig DummyController extends Plugin{}
one sig GeomagicTouch_plugin extends Plugin{}
one sig HomePosition extends Plugin{}

--events
abstract sig Event {}

-- events related to GeomagicTouch device
abstract sig GeomagicEvent extends Event {}
one sig GMTDeviceSignal extends GeomagicEvent {
	toRobotApp: GeomagicTouch -> LoadedPlugins}

--pred fromgmt 

-- events abd predicates related to RobotApp
abstract sig RobotAppEvent extends Event {}
one sig MsgToRobotApp extends RobotAppEvent{
	fromGeomagic: GeomagicTouch -> one LoadedPlugins,
	fromKinematics: KinematicModel -> one LoadedPlugins}

one sig MsgToKinematics extends RobotAppEvent{
	toKinematics: LoadedPlugins-> KinematicModel}

one sig MsgToRobotControl extends RobotAppEvent{
	toRobotControl: LoadedPlugins -> RobotControl}

pred SignalReceivedFromGMT [gmtsignal : GMTDeviceSignal, msgrobot: MsgToRobotApp]{
	gmtsignal.toRobotApp = msgrobot.fromGeomagic}

-- events and predicates related to Kinematics
abstract sig SolverEvent extends Event {}
one sig CoordinatesEvent extends SolverEvent{
	fromRobotApp: LoadedPlugins -> KinematicModel}

one sig AnglesEvent extends SolverEvent{
	toRobotApp:  KinematicModel -> LoadedPlugins}

pred MsgSentToKinematics [mtk: MsgToKinematics, ce: CoordinatesEvent]{
	mtk.toKinematics = ce.fromRobotApp}

pred AnglesSentToRobotApp [msgangles: AnglesEvent, msgrobot: MsgToRobotApp]{
	msgangles.toRobotApp = msgrobot.fromKinematics}

-- signatures describing the events relating to robot control 
abstract sig RobotControlEvent extends Event {}
one sig SignalFromRobotApp extends RobotControlEvent{
	fromRobotApp: LoadedPlugins -> RobotControl	
}
one sig SignalToRobot extends RobotControlEvent{
	toRobot: RobotControl -> RobotArm
}
pred MsgSentFromRobotAppToRobotControl [msgrc: MsgToRobotControl, sfra: SignalFromRobotApp]{
	msgrc.toRobotControl = 	sfra.fromRobotApp
}

pred SignalToRobot [robotArm: RobotArm]
{robotArm.finalAngleSignal = SignalToRobot}

one abstract sig Notification {} 
sig Yes extends Notification {}
sig No extends Notification {}


--High-level property ?????????????
pred CorrectAngleProperty [gmt : GeomagicTouch, angle : RobotControl] 
{gmt.input = Coordinate => angle.output = ArmAngle}

--pred RobotControlEvent [rce: RobotControl]
--{rce.f = MsgToOther => rce.robotMsgOut = SignalToRobot}


//pedal press/not press - later

--return the angles produced from a specific coordinate
fun getArmAngles[s: KinematicModel, c: Coordinate] : one (ArmAngle) { 
	s.solverResult[c]
}

--Facts.
--outputs should be in the range of solverResult
fact{
all o: RobotControl.output | one a: getArmAngles[KinematicModel,Coordinate] | o = a
}

--There is one kinematic model for each robot arm
fact {
    all r: RobotArm | one k: RobotModel | r.kinModel = k
}

--Each solver belongs to one kinematic model
fact {
   KinematicModel in RobotModel.*inverseKSolver
}

fact{
	Controller in RobotControl.*robotConfig
}

--all coordinates belong to GMT movements
fact {
	all c: Coordinate | all g : GeomagicTouch | c in g.input 
}

--for each of the c coordinates there exists an angle 
--and that angle is in the solver result 
fact {
	all c: Coordinate | one a: ArmAngle, s : KinematicModel| c->a in s.solverResult
}

fact {
	one Robot
	one RobotModel
	one GeomagicTouch
	one RobotControl
	one RobotArm
	one SolverPlugin
	one SolverFamily
	some Plugin
}

pred properties []{
	SignalReceivedFromGMT [GMTDeviceSignal, MsgToRobotApp]
	MsgSentToKinematics [MsgToKinematics, CoordinatesEvent]
	AnglesSentToRobotApp [AnglesEvent, MsgToRobotApp]
	MsgSentFromRobotAppToRobotControl [MsgToRobotControl, SignalFromRobotApp]
	SignalToRobot [RobotArm]
}

pred produceNotification[output : RobotControl.output] {
	output not in RobotModel.anglelimit 
	some notif : Notification | notif = Yes
}

--Not sure if properties should be called in a fact. 
--But we are assuming that all the events are handled correctly?
fact {
	#RobotModel.anglelimit > 2
	properties[]
	#RobotControl.output = 1
}

--assert if the arm angle created by movement is in the set of armangle limit
assert ArmAngleCorrect {
all a: RobotControl.output | a not in RobotModel.anglelimit 
implies produceNotification[a]
}
check ArmAngleCorrect for 4 but 9 Plugin

--What you should get as output:
-- solverResult[Coordinate1] is equal to ArmAngle0 unless ArmAngle0 is not in anglelimit
