module SurgeonRobot

open SurgeonBotFeatureSpec2

sig armangle extends ArmAngle{}
sig xyz_input_coordinate extends Coordinate{}

--plugins expected from a typical config file
one sig gmt_plugin extends GeomagicTouch_plugin{}
one sig hp_instance extends HomePosition{}
one sig clutch_instance extends Clutch_Plugin{}
one sig IKSolver_plugin extends SolverPlugin{}{
	solverfamily = IKSolver_family
}

--list of loaded plugins - get the list of plugins extracted from config file
one sig loaded_plugins_of_ extends LoadedPlugins {}{
	gmt_plugin + hp_instance + clutch_instance + IKSolver_plugin in loads
}

one sig IKSolver extends KinematicModel{}

--name of solver class
one sig IKSolver_family extends SolverFamily{}{
	calls = IKSolver
}

one sig FrankenVREP extends ArmType{}{
	inverseKSolver = IKSolver
}

one sig FrankenVREPArm extends RobotArm{}
{
	armside = Right_Side
	type = Cautery_Tissue_Grasper
	armModel = ExtendingSixDOF
}

one sig Right_Side extends Right{}{
	inverted = InvertTrue
}

one sig gmt extends GeomagicTouch {}{
	force = HapticsEnabled
}

one sig current_robot extends Robot {}{
	arms = FrankenVREPArm
}

check ArmAngleCorrect for 4 but 8 Plugin
