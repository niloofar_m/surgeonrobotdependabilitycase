module SurgeonRobot

-- A distinction between whether or not a solver family is capable of producing notification
-- or if the current robot arm DOES produce notification

--- Add details of effector
--- Add detail in 4 arm functions
--- Signatures for abilities of some mandatory plugins (Plus cautery)
--- What kind of effectors are available? -- 5 effectors
--- Sig roboteffector, extends to cautery grasper etc etc
--- left and right arm
--- left is not invert
--- right does invert 

--components

abstract sig ArmAngle, Coordinate, Controller {}

abstract sig Plugin {
	pluginInput: one Event,
	pluginOutput: one Event,
}

abstract sig SolverFamily{
	calls: one KinematicModel
}

abstract sig GeomagicTouch {
	input: some Coordinate,
--	: one AngleType
	gmtsignal : GeomagicTouchEvent,
	force: Notification
}

abstract one sig RobotApp {
	includes: set Plugin
}

abstract one sig LoadedPlugins {
	loads: set Plugin
}

abstract sig RobotModel {
	anglelimit: set ArmAngle, 
--set of all the arm angles that are less than limit
	inverseKSolver: one KinematicModel
}

abstract one sig Robot {
	arms: some RobotArm
}

abstract sig RobotArm
{
	kinModel: one RobotModel,
}

abstract sig SolverPlugin extends Plugin{
	solverfamily: one SolverFamily,
}

abstract sig KinematicModel{
	solverResult: Coordinate -> ArmAngle
}

abstract sig RobotControl{
	output: some ArmAngle,
	rcinput: RobotAppEvent,
}

one sig ButtonInterface extends Plugin{}
one sig Clutch extends Plugin{}
one sig GrasperLimits extends Plugin{}
one sig DummyController extends Plugin{}
one sig GeomagicTouch_plugin extends Plugin{}
one sig HomePosition extends Plugin{}

--events


abstract sig Event {}

-- events related to GeomagicTouch device
abstract sig GeomagicTouchEvent extends Event{}
one sig GeomagicOutput extends GeomagicTouchEvent{
	toRobotAppSignal: GeomagicTouch -> RobotApp,
	gmtOutput: one Notification
}

abstract sig RobotAppEvent extends Event{}
one sig GeomagicPluginOutput, 
	HomePositionOutput, 
	ClutchOutput extends RobotAppEvent{}
one sig KinematicsOutput extends RobotAppEvent{
	destination: RobotControl
}

pred inputToRobotApp [gte: GeomagicTouch, re: LoadedPlugins.loads]{
		re = GeomagicTouch_plugin &&	gte.gmtsignal = GeomagicOutput => 
										re.pluginInput = GeomagicOutput
}

pred GMTPluginToHP [arg1: LoadedPlugins.loads, arg2: LoadedPlugins.loads]{
	arg1 = GeomagicTouch_plugin && 
	arg2 = HomePosition &&
	arg1.pluginOutput = GeomagicPluginOutput 
						=> arg2.pluginInput = GeomagicPluginOutput
}

pred HPtoClutch [arg1: LoadedPlugins.loads, arg2: LoadedPlugins.loads]{
	arg1 = HomePosition && 
	arg2 = Clutch &&
	arg1.pluginOutput = HomePositionOutput 
		=> arg2.pluginInput = HomePositionOutput
}

pred ClutchtoKinematics [arg1: LoadedPlugins.loads, arg2: LoadedPlugins.loads]{
	arg1 = Clutch && 
	arg2 = SolverPlugin &&
	arg1.pluginOutput = ClutchOutput 
		=> arg2.pluginInput = ClutchOutput
}

pred KinematicstoRobotApp[arg1: LoadedPlugins.loads, arg2: RobotControl]{
	arg1 = SolverPlugin &&
	arg1.pluginOutput = KinematicsOutput
			=> arg2.rcinput = KinematicsOutput
}

--condition => formula1
--pluginInput: one Event,
--pluginOutput: one Event


--abstract sig GeomagicEvent extends Event {}
--one sig GMTDeviceSignal extends GeomagicEvent {
--	toRobotApp: GeomagicTouch -> LoadedPlugins}

--pred fromgmt 

-- events and predicates related to RobotApp
//abstract sig RobotAppEvent extends Event {}
//one sig MsgToRobotApp extends RobotAppEvent{
	//fromGeomagic: GeomagicTouch -> one LoadedPlugins,
	//fromKinematics: KinematicModel -> one LoadedPlugins}

//one sig MsgToKinematics extends RobotAppEvent{
	//toKinematics: LoadedPlugins-> KinematicModel}

//one sig MsgToRobotControl extends RobotAppEvent{
	//toRobotControl: LoadedPlugins -> RobotControl}

--pred SignalReceivedFromGMT [gmtsignal : GMTDeviceSignal, msgrobot: MsgToRobotApp]{
--	gmtsignal.toRobotApp = msgrobot.fromGeomagic}

-- events and predicates related to Kinematics
--abstract sig SolverEvent extends Event {}
--one sig CoordinatesEvent extends SolverEvent{
	--fromRobotApp: LoadedPlugins -> KinematicModel}

//one sig AnglesEvent extends SolverEvent{
	//toRobotApp:  KinematicModel -> LoadedPlugins}

//pred MsgSentToKinematics [mtk: MsgToKinematics, ce: CoordinatesEvent]{
	//mtk.toKinematics = ce.fromRobotApp}

//pred AnglesSentToRobotApp [msgangles: AnglesEvent, msgrobot: MsgToRobotApp]{
	//msgangles.toRobotApp = msgrobot.fromKinematics}

-- signatures describing the events relating to robot control 
//abstract sig RobotControlEvent extends Event {}

//one sig SignalToRobot extends RobotControlEvent{
	//toRobot: RobotControl -> RobotArm
//}
//pred MsgSentFromRobotAppToRobotControl [msgrc: MsgToRobotControl, sfra: SignalFromRobotApp]{
	//msgrc.toRobotControl = 	sfra.fromRobotApp
//}

//pred SignalToRobot [robotArm: RobotArm]
//{robotArm.finalAngleSignal = SignalToRobot}

one abstract sig Notification {} 
sig Yes extends Notification {}
sig No extends Notification {}

--High-level property ?????????????
pred CorrectAngleProperty [gmt : GeomagicTouch, angle : RobotControl] 
{gmt.input = Coordinate => angle.output = ArmAngle}

--pred RobotControlEvent [rce: RobotControl]
--{rce.f = MsgToOther => rce.robotMsgOut = SignalToRobot}


//pedal press/not press - later

--return the angles produced from a specific coordinate
fun getArmAngles[s: KinematicModel, c: Coordinate] : one (ArmAngle) { 
	s.solverResult[c]
}

--Facts.
--outputs should be in the range of solverResult
fact{
all o: RobotControl.output | one a: getArmAngles[KinematicModel,Coordinate] | o = a
}

--There is one kinematic model for each robot arm
fact {
    all r: RobotArm | one k: RobotModel | r.kinModel = k
}

--Each solver belongs to one kinematic model
fact {
   KinematicModel in RobotModel.*inverseKSolver
}

--all coordinates belong to GMT movements
fact {
	all c: Coordinate | all g : GeomagicTouch | c in g.input 
}

--for each of the c coordinates there exists an angle 
--and that angle is in the solver result 
fact {
	all c: Coordinate | one a: ArmAngle, s : KinematicModel| c->a in s.solverResult
}

fact {
	one Robot
	one RobotModel
	one GeomagicTouch
	one RobotControl
	one RobotArm
	one SolverPlugin
	one SolverFamily
}

pred properties []{
	inputToRobotApp [GeomagicTouch, LoadedPlugins.loads]
	GMTPluginToHP [LoadedPlugins.loads, LoadedPlugins.loads]
	HPtoClutch [LoadedPlugins.loads, LoadedPlugins.loads]
	ClutchtoKinematics [LoadedPlugins.loads, LoadedPlugins.loads]
	KinematicstoRobotApp[LoadedPlugins.loads, RobotControl]
}

pred produceNotification[output : RobotControl.output] {
	output not in RobotModel.anglelimit 
	some notif : Notification | notif = Yes
}

--Not sure if properties should be called in a fact. 
--But we are assuming that all the events are handled correctly?
fact {
	#RobotModel.anglelimit > 2 //up to four
	#RobotControl.output = 1
	properties[]
}

--assert if the arm angle created by movement is in the set of armangle limit
assert ArmAngleCorrect {
all a: RobotControl.output | a not in RobotModel.anglelimit 
implies produceNotification[a]
}
check ArmAngleCorrect for 4 but 9 Plugin

--What you should get as output:
-- solverResult[Coordinate1] is equal to ArmAngle0 unless ArmAngle0 is not in anglelimit
