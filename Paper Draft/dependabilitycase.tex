\section{Dependability case} \label{sec-case}

A Dependability case is an explicit, end-to-end argument that a system satisfies a critical property \cite{near2011lightweight}. It is necessary to provide concrete evidence that the property is satisfied. One way to produce such a concrete evidence is by means of software verification methods \cite{pernsteiner2016investigating}. In our approach, we first define our safety-critical property and we continue to explain about the necessary steps to build a dependability case.

\subsection{Construction of the dependability case} \label{construction}
We define our safety-critical property as Correct Angle Property, which is described as follows:\\
\newline
\indent \textbf{Correct Angle Property:} As the surgeon moves the control device, the actual position of the robot arm in the workspace should be the exact same position as what the surgeon expects.\\
\newline
\indent Modeling and verifying an entire complex software system such as the robot control software requires a lot of precision and can be quite expensive. Thus, we utilize the concept of trusted bases \cite{kang2010dependability} to realize the components that are directly involved in satisfying this critical property. We use the Problem Frames \cite{jackson2001problem} approach to articulate the structure of the system and the underlying relationship thereof to the requirements \cite{kang2010dependability}. This approach has a few key concepts which are briefly described below.\\
\indent The problem frames \cite{jackson2001problem} approach, distinguishes the existing parts of the world (\textbf{application domains}) from the components that need to be built to solve the problem (\textbf{machines}).\\
\indent In problem frames, a property on a software is the specification that the software realized must fulfill.\\
Using these concepts, we have constructed a problem diagram for the Correct Angle Property. In this diagram, which is shown in Figure \ref{fig:fig2}, a box is a representation of a system part that is involved in satisfying the property; the edges between the boxes represent a shared phenomenon that is used for the interaction between the parts.

\begin{figure}
\centering
\includegraphics[width = \columnwidth]{problemdiagram.pdf} % importing figure
\caption {Problem Diagram for the Correct Angle Property}
\label{fig:fig2} % labeling to refer it inside the text
\end{figure}

The next step is to assign the properties to the parts. Using a property-part diagram, we show the dependencies between the properties and the parts. This diagram, as shown in Figure \ref{fig:fig3}, shows that each part of the system, while satisfying its own requirement and specification - such as sending coordinates from GeomagicTouch software layer to the RobotApp layer - also takes part in satisfying the overall safety-critical property. The sequence of events in the system corresponds to the relationship between properties on the system.\\
\indent Using lightweight formal methods, we can model the relationship between the properties involved in satisfying the safety requirement, to establish that these properties can work together to ensure that the property holds. This specification can be used as a reusable model to which all the extracted models for different robots must conform. We will explain more about the model extraction in the related section.

\begin{figure*}
\centering
\includegraphics[width=\textwidth]{propertypart.pdf} % importing figure
\caption {Property-Part Diagram for the Correct Angle Property}
\label{fig:fig3} % labeling to refer it inside the text
\end{figure*}

\subsection{Building a case for Correct Angle Property}\label{buidlingacase}

Our objective is to argue that when a surgeon initiates a movement using the Geomagic Touch Device in the control workspace, the desired robot arm position is going to be the same as the actual robot arm position.\\
\indent Having identified the properties on the system components that are involved in satisfying the safety-critical property, we perform a code analysis to determine which parts of the code are involved in satisfying the requirements of each individual property on a part, and we then use code analysis to extract the needed information to create individual Alloy models that conform to the specification that was written using the property-part diagram.\\
The robot control system is a plug-in based, event-driven system. The user can load a set of plug-ins in the graphical user interface when the program starts, and the correct combination of plug-ins makes it possible to control a robot arm. The system is designed using Model-View-ViewModel pattern in C\#, and each of these plug-ins give the user a view that they can use and modify the options within them. The plugins communicate using a messaging system, passing events to one another as the program works. An event is a message sent by an object to signal the occurrence of an action.  The user can also add motor configurations that correspond to the robot arm, and these configurations specify which motors receive the calculated angle values in the software layer.\\
\indent In a typical scenario, the surgeon moves the handle on the Touch device to move the robot arm to a specific position. The Geomagic Touch software components receive the coordinates of endpoint of the handle, which specify where in the robot workspace the robot arm should move to. The coordinates are then sent to a plug-in in the RobotApp layer, whose responsibility is to add the coordinates that have been sent from the Geomagic Touch component into the inputs array that can be modified by other plug-ins in that layer. As mentioned earlier, these plugins interact through a messaging system and send information to one another using that system.\\
\indent We are concerned with the plug-ins that are absolutely necessary for controlling the robot arm, such as GeomagicTouch, Clutch, HomePosition, and a solver plugin. As mentioned in the last paragraph, the GeomagicTouch plugin is responsible for bringing the coordinates into the RobotApp layer. Clutch and HomePosition are plug-ins that work when a pedal button that corresponds to them is pressed. When the Clutch pedal button is pressed, the RobotApp sends the coordinates to the Clutch plugin, which is designed for when the surgeon runs into the physical limits of the Touch device before the physical limits of the robot. To address this issue, the Clutch plug-in is used so that the surgeon would be able to stop the robot from moving and reposition the endpoint of Touch device to a point where it's easier to use. The same goes for HomePosition plug-in, when the HomePosition pedal is pressed by the surgeon, she can reset the starting point of the coordinate system with the GeomagicTouch device endpoint.\\
\indent After the Clutch and Home plug-in are used and the array of inputs is updated, the coordinates are sent to the appropriate Kinematics plug-in, which recognizes the kinematic model of the current robot arm. In the Kinematics class these 8 Kinematic models are defined:

\begin{itemize}
  \item CombinedBot
  \item CoupledShoulder3DOF
  \item CoupledShoulderAndElbow3DOF
  \item FrankenBot
  \item IKSolver
  \item IKSolver5DOF
  \item TwoArmCoupledShoulder
  \item TwoArmCoupledShoulder3DOF
\end{itemize}

\indent These kinematic classes each include a unique solver for the inverse kinematic problem of a family of robotic arms. All of these classes have one getJointAngles() function that takes in a coordinate, and calculates the joint angles corresponding to that coordinate, and send them back to the solver plug-in. The specifications of several robot arms are also defined in this component, each of them inherited from some Kinematics model. For instance, LouBizzle's kinematic model is IKSolver, so when we are using the LouBizzle arm and selecting the arm in the program, IKSolver is the solver that produces the joint angles of the arm corresponding to the coordinate.\\
\indent Subsequently, the calculated values are then converted to serial data and sent to the motors specified in the motor configurations panel of the software.\\
\indent As this system is designed to control various different robots, there are specific plug-ins that are used for when the surgeon is handling each specific robot arm, but there are also some plug-ins that are always necessary, as we mentioned before. There are also similarities between the properties of different robots and some of the used components are constant. In the next section, we talk about how we modeled the property regardless of what kind of robot arm is being used, and how we're going to use the extracted information from our code analysis to automatically build individual unique models for each robot that can be analyzed to realize if the property will hold.\\
