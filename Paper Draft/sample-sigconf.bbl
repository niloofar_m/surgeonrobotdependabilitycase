\begin{thebibliography}{10}

\bibitem{kang2001robotic}
Hyosig Kang and John~T Wen.
\newblock Robotic assistants aid surgeons during minimally invasive procedures.
\newblock {\em IEEE Engineering in Medicine and Biology Magazine},
  20(1):94--104, 2001.

\bibitem{knight2002safety}
John~C Knight.
\newblock Safety critical systems: challenges and directions.
\newblock In {\em Software Engineering, 2002. ICSE 2002. Proceedings of the
  24rd International Conference on}, pages 547--550. IEEE, 2002.

\bibitem{jackson2009direct}
Daniel Jackson.
\newblock A direct path to dependable software.
\newblock {\em Communications of the ACM}, 52(4):78--88, 2009.

\bibitem{jackson2002alloy}
Daniel Jackson.
\newblock Alloy: a lightweight object modelling notation.
\newblock {\em ACM Transactions on Software Engineering and Methodology
  (TOSEM)}, 11(2):256--290, 2002.

\bibitem{ernst2015toward}
Michael~D Ernst, Dan Grossman, Jon Jacky, Calvin Loncaric, Stuart Pernsteiner,
  Zachary Tatlock, Emina Torlak, and Xi~Wang.
\newblock Toward a dependability case language and workflow for a radiation
  therapy system.
\newblock In {\em LIPIcs-Leibniz International Proceedings in Informatics},
  volume~32. Schloss Dagstuhl-Leibniz-Zentrum fuer Informatik, 2015.

\bibitem{jackson2001problem}
MA~Jackson.
\newblock Problem frames: Analysing and structuring software development
  problems pdf.
\newblock 2001.

\bibitem{cubrichthesis}
Lou Cubrich.
\newblock Design of a flexible control platform and miniature in vivo robots
  for laparo-endoscopic single-site surgeries, 2016.

\bibitem{fei2001safety}
Baowei Fei, Wan~Sing Ng, Sunita Chauhan, and Chee~Keong Kwoh.
\newblock The safety issues of medical robotics.
\newblock {\em Reliability Engineering \& System Safety}, 73(2):183--192, 2001.

\bibitem{hapticdevice}
https://www.3dsystems.com/haptics-devices/touch.
\newblock \url{https://www.3dsystems.com/haptics-devices/touch}.

\bibitem{near2011lightweight}
Joseph~P Near, Aleksandar Milicevic, Eunsuk Kang, and Daniel Jackson.
\newblock A lightweight code analysis and its role in evaluation of a
  dependability case.
\newblock In {\em Proceedings of the 33rd International Conference on Software
  Engineering}, pages 31--40. ACM, 2011.

\bibitem{pernsteiner2016investigating}
Stuart Pernsteiner, Calvin Loncaric, Emina Torlak, Zachary Tatlock, Xi~Wang,
  Michael~D Ernst, and Jonathan Jacky.
\newblock Investigating safety of a radiotherapy machine using system models
  with pluggable checkers.
\newblock In {\em International Conference on Computer Aided Verification},
  pages 23--41. Springer, 2016.

\bibitem{kang2010dependability}
Eunsuk Kang and Daniel Jackson.
\newblock Dependability arguments with trusted bases.
\newblock In {\em Requirements Engineering Conference (RE), 2010 18th IEEE
  International}, pages 262--271. IEEE, 2010.

\bibitem{CodedUI}
Codedui.
\newblock \url{https://msdn.microsoft.com/en-us/library/dd286726.aspx}.

\bibitem{NAP11923}
National~Research Council.
\newblock {\em Software for Dependable Systems: Sufficient Evidence?}
\newblock The National Academies Press, Washington, DC, 2007.

\bibitem{McDermid:2001:SSW:563780.563781}
John~A McDermid.
\newblock Software safety: Where's the evidence?
\newblock In {\em Proceedings of the Sixth Australian Workshop on Safety
  Critical Systems and Software - Volume 3}, SCS '01, pages 1--6, Darlinghurst,
  Australia, Australia, 2001. Australian Computer Society, Inc.

\bibitem{kelly2004goal}
Tim Kelly and Rob Weaver.
\newblock The goal structuring notation--a safety argument notation.
\newblock In {\em Proceedings of the dependable systems and networks 2004
  workshop on assurance cases}, page~6. Citeseer, 2004.

\bibitem{nair2014extended}
Sunil Nair, Jose~Luis De~La~Vara, Mehrdad Sabetzadeh, and Lionel Briand.
\newblock An extended systematic literature review on provision of evidence for
  safety certification.
\newblock {\em Information and Software Technology}, 56(7):689--717, 2014.

\bibitem{graydon2007assurance}
Patrick~J Graydon, John~C Knight, and Elisabeth~A Strunk.
\newblock Assurance based development of critical systems.
\newblock In {\em Dependable Systems and Networks, 2007. DSN'07. 37th Annual
  IEEE/IFIP International Conference on}, pages 347--357. IEEE, 2007.

\bibitem{wagner2010case}
Stefan Wagner, Bernhard Schatz, Stefan Puchner, and Peter Kock.
\newblock A case study on safety cases in the automotive domain: Modules,
  patterns, and models.
\newblock In {\em Software Reliability Engineering (ISSRE), 2010 IEEE 21st
  International Symposium on}, pages 269--278. IEEE, 2010.

\bibitem{Ridderhof2007EstablishingEF}
Willem Ridderhof, Hans-Gerhard Gro\ss, and Heiko D{\"o}rr.
\newblock Establishing evidence for safety cases in automotive systems - a case
  study.
\newblock In {\em SAFECOMP}, 2007.

\bibitem{Graydon2012Conformance}
P.~Graydon, I.~Habli, R.~Hawkins, T.~Kelly, and J.~Knight.
\newblock Arguing conformance.
\newblock {\em IEEE Software}, 29(3):50--57, May 2012.

\bibitem{Denney2012Perspectives}
E.~Denney, G.~Pai, and I.~Habli.
\newblock Perspectives on software safety case development for unmanned
  aircraft.
\newblock In {\em IEEE/IFIP International Conference on Dependable Systems and
  Networks (DSN 2012)}, pages 1--8, June 2012.

\bibitem{bourdil2016integrating}
Pierre-Alain Bourdil, Silvano Dal~Zilio, and Eric Jenn.
\newblock Integrating model checking in an industrial verification process: a
  structuring approach.
\newblock 2016.

\bibitem{sullivan2004software}
Kevin Sullivan, Jinlin Yang, David Coppit, Sarfraz Khurshid, and Daniel
  Jackson.
\newblock Software assurance by bounded exhaustive testing.
\newblock {\em ACM SIGSOFT Software Engineering Notes}, 29(4):133--142, 2004.

\bibitem{gacek2014resolute}
Andrew Gacek, John Backes, Darren Cofer, Konrad Slind, and Mike Whalen.
\newblock Resolute: an assurance case language for architecture models.
\newblock In {\em ACM SIGAda Ada Letters}, volume~34, pages 19--28. ACM, 2014.

\bibitem{Tuan2010Pace}
L.~A. Tuan, M.~C. Zheng, and Q.~T. Tho.
\newblock Modeling and verification of safety critical systems: A case study on
  pacemaker.
\newblock In {\em 2010 Fourth International Conference on Secure Software
  Integration and Reliability Improvement}, pages 23--32, June 2010.

\bibitem{brunel2014safety}
Julien Brunel and David Chemouil.
\newblock Safety and security assessment of behavioral properties using alloy.
\newblock In {\em International Conference on Computer Safety, Reliability, and
  Security}, pages 251--263. Springer, 2014.

\bibitem{brunel2014formal}
Julien Brunel, Laurent Rioux, St{\'e}phane Paul, Anthony Faucogney, and
  Fr{\'e}d{\'e}rique Vall{\'e}e.
\newblock Formal safety and security assessment of an avionic architecture with
  alloy.
\newblock {\em arXiv preprint arXiv:1405.1113}, 2014.

\bibitem{jackson2009property}
Daniel Jackson and Eunsuk Kang.
\newblock Property-part diagrams: A dependence notation for software systems.
\newblock Institute of Electrical and Electronics Engineers, 2009.

\end{thebibliography}
