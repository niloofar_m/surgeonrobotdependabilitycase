

\section{Approach}\label{sec-approach}
In this section we describe a formal model of the critical components of the system that are involved in satisfying the specified safety-critical property. We use the Alloy specification language to model the safety-critical property, and check whether or not it always holds.
Alloy is a lightweight formal system modeling language amenable to automatic analyses \cite{jackson2002alloy}. Alloy's tool, the Alloy Analyzer, relies on a SAT-solver that given a constraints of a model, finds instances in a limited scope which satisfy those constraints. The Alloy analyzer can be used to explore the models by generating sample structures, and to check properties of the model by generating counter examples.\\
\begin{comment}
	We go over the main concepts of the language. Each Alloy file contains several declarations, the order of which are not relevant. There are different kinds of statements in Alloy models, such as Signatures, Facts, Predicates, Functions and Assertions.\\
	 \textbf{Signature} introduces a structural concept (similar or a Class) that defines the vocabulary of a model by creating new sets. Each signature has fields that could be interpreted as attributes of a class \cite{brunel2014safety}. Relations between different sets can also be defined inside these signatures.\\
	\indent \textbf{Facts} are constraints that are assumed to always hold in the model, and \textbf{Predicates} on the other hand are constraints which always return either True or False as their result. \textbf{Functions} are expressions that return results, which are relations or sets of some given type. \textbf{Assertions} also produce either true or false and can be checked in the Alloy Analyzer. Assertion differs from a fact in that the analyzer can check an assertion to see if it is true for all the instances found in a scope, and if not, show us instances in which the assertion is not true. On the other hand, the analyzer assumes that facts are always true.\\
\end{comment}

We model the surgical robot software system in Alloy, using the constraints and the facts captured from the system and the relationships between its components.\\
\indent We develop an Alloy model to verify the Correct Angle property of our robotic surgeon system, and we model the components that are involved in satisfying this property, and moving the robot arm to the desired position.\\
\indent In order to build this model, we used the components of the system that we recognized as being involved in satisfying this property.\\
\indent For the robotic surgeon system, we first define the input and output, and the components involved in the system as signatures. The input of this system is a set of coordinates, and the output is a set of arm angles for the robot arm that correspond to the coordinate. There are also plugins in the system, and the necessary ones for surgeon use are defined as signatures of the system. Specifically, the SolverPlugin signature is created and the field solverfamily is defined to show the relationship between a SolverPlugin with one SolverFamily (i.e. Kinematics solver plug-in to SingleArm solver family). The components are also defined as signatures in the system. There is one GeomagicTouch device used to control each robotic arm, so for robots with only one arm we would only have one such device. The inputs are given to the system using the position of the GeomagicTouch device, which is defined by the field "input" in the GeomagicTouch signature.\\
\indent The system has a layer that includes the plug-ins which is called RobotApp, and this component includes a set of plug-ins. We also define a LoadedPlugins signature, to specify what plugins are actually loaded in each configuration. We define a SolverFamily signature, since each solver Plug-in is defined for a specific solver family (SingleArm, TwoArm, or IKSolver) and each SolverFamily calls one KinematicModel based on the chosen arm. We define our KinematicModel signature, and within that signature there's a field that specifies the relation between a coordinate and arm angles, which are produced by a kinematic model. We define a Robot signature and field to specify the name of the robot arm, and we also define a RobotArm signature, and we can specify the robot arm, arm type, or the end effector type for each RobotArm. ArmType is defined separately, and we use the fields in that signature to specify a set of angle limits related to a specific arm type. The angle limits are the maximum angles that the joints can't physically move beyond. For instance, if the arm we are using has 6 joints, then we have 6 joint angle limits for that specific arm type. Each arm type also has one unique kinematic model, which is defined as a relation in the ArmType signature. We also define a RobotControl signature, which corresponds to the layer of the system which receives the serial data from the plug-ins. We define the output of the system as the arm angles that are sent to RobotControl. We also define the 5 available types of effectors, the left and right arm sides, and the different pedal functions that the surgeon can use during surgey as independent signatures.\\

Figure \ref{fig-model-1} shows the signatures that are components of the system.
\nolinebreak
\begin{figure}
\begin{alloy}

abstract sig ArmAngle, Coordinate {}
abstract sig Plugin {}

abstract sig GeomagicTouch {
	input: one Coordinate}

abstract one sig RobotApp {
	includes: some Plugin}

abstract one sig LoadedPlugins {
	loads: some Plugin}

abstract sig SolverFamily{
	calls: one KinematicModel}

--specifies the solver
abstract sig KinematicModel{
	solverResult: Coordinate -> ArmAngle}

abstract one sig Robot {
	arms: some RobotArm}

abstract sig RobotArm{
	armside: one Side,
	armModel: one ArmType,
	effectorType: one EffectorType}

--anglelimit: set of all the arm angles that
--are less than limit
abstract sig ArmType {
	anglelimit: set ArmAngle,
	inverseKSolver: one KinematicModel}

abstract sig RobotControl{
	output: set ArmAngle,}

one sig Clutch_Plugin extends Plugin{}
one sig GeomagicTouch_plugin extends Plugin{}
one sig HomePosition extends Plugin{}
one sig GrasperLimits extends Plugin{}
one sig Scale extends Plugin{}
one sig DummyController extends Plugin{}
one sig ButtonInterface extends	Plugin{
	setButtonForPedal : some PedalButton
}
abstract sig SolverPlugin extends Plugin{
	solverfamily: one SolverFamily,}

abstract sig Side{}
lone sig Left extends Side{}
lone sig Right extends Side{}

abstract sig EffectorType{}
lone sig Cautery_Tissue_Grasper extends EffectorType{}
lone sig Cautery_Shears extends EffectorType{}
lone sig Cautery_Hook extends EffectorType{}
lone sig Tissue_Grasper extends EffectorType{}
lone sig Shears extends EffectorType{}

abstract sig PedalFunction{}
one sig ClutchButton extends PedalFunction{}
one sig HPButton extends PedalFunction{}
one sig ScaleButton extends PedalFunction{}
one sig CauteryButton extends PedalFunction{}

--each button is assigned to one function
sig PedalButton{
	assigned: one PedalFunction
}

\end{alloy}
\caption{A snippet of the Alloy model featuring the components of the system}
\label{fig-model-1}
\end{figure}

\begin{figure}
\begin{alloy}
--return the angles produced from a specific coordinate
fun getArmAngles[s: KinematicModel, c: Coordinate] : one (ArmAngle) {
		s.solverResult[c]}
--Facts.
--outputs should be in the range of solverResult
fact{
	all o: RobotControl.output | one a: getArmAngles[KinematicModel,Coordinate] | o = a}

--There is one kinematic model for each robot arm
fact {
	all r: RobotArm | one k: ArmType | r.armModel = k}

--The solver should be in the armtype solver set
fact {
	KinematicModel in ArmType.*inverseKSolver}

--all coordinates belong to GMT movements
fact {
	all c: Coordinate | all g : GeomagicTouch | c in g.input}

--for each of the c coordinates there exists an angle
--and that angle is in the solver result
fact {
	all c: Coordinate | some a: ArmAngle, s : KinematicModel|
	c->a in s.solverResult}

--all Plugins belong to RobotApp
fact {
	all p: Plugin | one r: RobotApp | p in r.includes}

--if the cautery effector is used, scale can't be used
--if the non-cautery tool is used, Grasper limits
--should be added to loads
--and Cautery button shouldn't be assigned
fact{
	(RobotArm.effectorType = Cautery_Tissue_Grasper or
	RobotArm.effectorType = Cautery_Shears or
	RobotArm.effectorType = Cautery_Hook)
	=> (ScaleButton not in PedalButton.assigned &&
	CauteryButton in PedalButton.assigned &&
	GrasperLimits not in LoadedPlugins.loads &&
	Scale not in LoadedPlugins.loads)
	else
	(CauteryButton not in PedalButton.assigned &&
		ScaleButton in PedalButton.assigned &&
		GrasperLimits in LoadedPlugins.loads)}

fact{
	ScaleButton in PedalButton.assigned
	=> Scale in LoadedPlugins.loads}

fact{
	ClutchButton + HPButton in PedalButton.assigned}

\end{alloy}
\caption{Facts defined in the Alloy model to provide constraints}
\label{fig-model-3}
\end{figure}
\indent We then define a few facts about the system to constraint the model in order to get a true sense of how the system works, as shown in Figure \ref{fig-model-3}. We make sure that the outputs that are produced for the RobotArm are ArmAngles that are in the solverResult relation, which means that an output which has not been calculated by the solver should not exist. We ensure that there is one KinematicModel for each RobotArm, and that each solver belongs to one KinematicModel solver. All the coordinates that exist in the model belong to a relation that is in GeomagicTouch, as we can't have coordinates in real life surgery without using the GeomagicTouch device. We then add the fact that states that for all the coordinates that exist in the model, there is more than one arm angle and exactly one solver, and there is a relationship between that coordinate and the arm angle that's specified in the solverResult relation (from the KinematicModel signature). We then add a fact that states that all plug-ins belong to the RobotApp. There are also some constraints about the end effectors, pedal buttons and what are the plugins that should be loaded with some of the effectors. There are 3 pedal buttons, two of which are always assigned to Clutch and HomePosition buttons, so there is one button which we can assign another function to. There are 5 available end effectors for the robot arms, and we categorize them into cautery effectors and non-cautery effectors for explaining these constraints in a simpler way. If the effector connected to the arm is of cautery type, then the one unassigned pedal button should be assigned to cautery function. Therefore, the pedal button can't be set as scale, and the Scale plug-in should not be the set of loaded plug-ins. While using cautery effectors, we can't use the GrasperLimits plug-in, so we have to make sure it's not in the set of loaded plug-ins. On the other hand, if the end effector is non-cautery, the Scale button is assigned to the pedal, and the GrasperLimits and Scale plug-in should be in the set of loaded plug-ins.  Another fact states that if we are using the Scale button, then a scale plug-in should be in the set of loaded plug-ins.\\
\indent In order to check the safety critical property, we assert that all the output angles produced by the solver fall into the set of angle limit, because otherwise the position of the robot arm wouldn't be the same as what is desired. The alloy analyzer finds counterexamples for individual models in which the output angle does not belong in the angle limit set, and therefore the output angle wouldn't be the one we expected from the system. Figure \ref{fig-model-4} shows the assertion that is used to check this property.\\
\indent After running the check command for the assertion, the Alloy analyzer finds instances in which the property does not hold, as the output is not in the set of anglelimit. After running this check on all the individual models, we realized that this could be a potential problem for all the robot arms, and we asked the engineers about whether they considered any solution for this issue. We were then informed that if the output angle is not the same as the desired angle, there exists a mechanism to alert the surgeon that their requested position is outside of the robot's arm reach. Some of the robot arms have a feedback ability, and in some of kinematic models, a haptic force will be produced the instance the surgeon tries to move the robot beyond its reach. We then modified our model to add this mechanism to it. We define a new signature, called HapticFeedback, which is an abstract signature and is extended to HapticsEnabled and HapticsDisabled. There is one instance of each of these signatures, because each of the robot models either have the ability of producing a feedback force, or they don't. We also add a predicate, defining when the force should be produced, and when the HapticFeedback should be HapticsEnabled. The assertion is also modified, because now we want to find instances in which the violation happens and the output angle is not in the set of angle limit, but a feedback force is not produced, and the surgeon is not notified of the fact that the expected angle is outside of the angle limits of the robot arm.\\

\begin{figure}
\begin{alloy}
assert ArmAngleCorrect {
properties[],
all a: RobotControl.output | a in ArmType.anglelimit
} check ArmAngleCorrect for 4 but 9 Plugin
\end{alloy}
\caption{An assertion to check the property}
\label{fig-model-4}
\end{figure}

The modified parts of the Alloy model are shown in Figure \ref{fig-model-revised}. After running that model to realize if there still could be real violations, we realize that for some robot models there is no mechanism to notify the surgeon of reaching the robot arm limits. For instance, the individual model for the FrankenVREP robot arm produced the counterexample partially shown in Figure \ref{fig:fig9}. It can be seen in the counter example that the solver has produced 4 arm angles, one for each joint of the FrankenVREP arm, and 'armangle4' is not in the set of anglelimits, but it is still sent to the RobotControl as an output angle. When this scenario happens, the arm moves as much as the physical limitation of it alloys, but the one of the joint angles are not equal to what has been calculated, so the position of the arm is not what the surgeon was expecting. The system does not produce the HapticFeedback for this robot, so if the violation happens, the surgeon does not get notified, and may try to push the robot beyond its physical limits again.\\

\begin{figure}
\begin{alloy}
abstract sig HapticFeedback{}
one sig HapticsEnabled extends HapticFeedback{}
one sig HapticsDisabled extends HapticFeedback{}

sig GeomagicTouch {
  input: some Coordinate,
	force: HapticFeedback}

pred ProduceFeedback[output : RobotControl.output] {
	output not in ArmType.anglelimit
	some notification : GeomagicTouch.force |
	notification = HapticsEnabled
}

assert ArmAngleCorrect {
all a: RobotControl.output | a not in ArmType.anglelimit
implies ProduceFeedback[a]
} check ArmAngleCorrect for 4 but 9 Plugin
\end{alloy}
\caption{Modified parts of the Alloy model}
\label{fig-model-revised}
\end{figure}

\begin{figure}
\includegraphics[width = \columnwidth]{CounterExample.png}
\caption{Counterexample for the modified model}
\label{fig:fig9}
\end{figure}

The Alloy meta model was a suitable way to describe the system specification and check the property in the system. We used code analysis to extract information about each individual robot arm from the code, to create individual models of each arm, and we checked the assertion on each model. The necessary information about each individual arm that was extracted from the code were the name of the robot arm model, the solver for that specific robot arm, the number of joints, and whether or not the robot has the Haptic feedback ability. With this information, we could build the individual models and realize which ones would produce the counterexamples. After identifying the robot arms that would violate the property, we then tested the control software for those robot arms to identify whether the violations were real or not.
